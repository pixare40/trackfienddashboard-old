# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpRequest
from django.shortcuts import render
import datetime
import pymongo
from .forms import NewsArticle
from TrackFiendDashboard import settings

def scraped_news(request):
    assert isinstance(request, HttpRequest)
    mongo_uri = settings.MONGO_URI
    mongo_database = settings.MONGO_DATABASE
    news_collection_name = "news"

    client = pymongo.MongoClient(mongo_uri)
    db = client[mongo_database]

    scraped_news = db[news_collection_name].find().sort('_id', pymongo.DESCENDING)
    client.close()

    return render(
        request,
        'admin/news/scraped_news.html',
        {'news':scraped_news})

def create_news_article(request):
    form = NewsArticle()
    return render(
        request,
        'admin/news/create_news_article.html', {'form': form})

def save_news_article(request):
    mongo_uri = settings.MONGO_URI
    mongo_database = settings.MONGO_DATABASE
    news_collection_name = "news"

    client = pymongo.MongoClient(mongo_uri)
    db = client[mongo_database]
    current_datetime = datetime.datetime.now().strftime('%a %b %d %H:%M:%S +0000 %Y')
    if request.method == 'POST':
        form = NewsArticle(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            news_item = dict()
            news_item['title'] = form_data['title']
            news_item['tags']= form_data['tags'].split(',')
            news_item['news_item_image'] = form_data['news_item_image']
            news_item['summary'] = form_data['summary']
            news_item['item_type'] = form_data['item_type']
            news_item['news_story'] = form_data['news_story']
            news_item['date_fetched'] = current_datetime
            news_item['date_created'] = current_datetime
            news_item['url_to_story'] = form_data['url_to_story']
            news_item['source'] = 'TrackFiend'

            db[news_collection_name].insert(news_item)

            return scraped_news(request)
    else:
        return create_news_article(request)