from django import forms
from ckeditor.widgets import CKEditorWidget

ARTICLE_FIELD_CHOICES = (
    ('article', 'Article'),
    ('video', 'Video'),
    ('external_video', 'External Video'),
    ('internal_article','Track Fiend Article')
)

class NewsArticle(forms.Form):
    tags = forms.CharField()
    news_item_image = forms.URLField()
    title = forms.CharField(max_length = 100, strip=True)
    summary = forms.CharField()
    item_type = forms.ChoiceField(required=True, choices = ARTICLE_FIELD_CHOICES, label='Article Type')
    news_story = forms.CharField(widget=CKEditorWidget(config_name='default'), label='News Story')
    url_to_story = forms.URLField()
