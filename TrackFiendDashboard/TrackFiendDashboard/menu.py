"""
This file was generated with the custommenu management command, it contains
the classes for the admin menu, you can customize this class as you want.

To activate your custom menu add the following to your settings.py::
    ADMIN_TOOLS_MENU = 'TrackFiendDashboard.menu.CustomMenu'
"""

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from admin_tools.menu import items, Menu


class CustomMenu(Menu):
    """
    Custom Menu for TrackFiendDashboard admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.Bookmarks(),
            items.AppList(
                _('Applications'),
                exclude=('django.contrib.*',)
            ),
            items.AppList(
                _('Administration'),
                models=('django.contrib.*',)
            ),
            items.MenuItem(_('Scraping Hub'), _('scraping')),
            items.MenuItem('Analytics Test', 'analytics_test'),
            items.MenuItem('Content Hub', 'news',
                           children = [
                               items.MenuItem('Create Article', 'create-news'),
                               items.MenuItem('Create Promotions', 'create-promotions')
                               ]
                           ),
            items.MenuItem('Twitter Hub', 'scraping/twitter_hub'),
            items.MenuItem('Google', 'scraping/google')
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)
