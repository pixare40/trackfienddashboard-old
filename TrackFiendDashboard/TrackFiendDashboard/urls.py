"""
Definition of urls for TrackFiendDashboard.
"""

from datetime import datetime
from django.conf.urls import url
import django.contrib.auth.views

import app.forms
import app.views
import news.views
import analyticshub.views

# Uncomment the next lines to enable the admin:
from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^contact$', app.views.contact, name='contact'),
    url(r'^about', app.views.about, name='about'),
    url(r'^all-posts/(?P<page>\d+)$', app.views.all_posts, name="all_posts"),
    url(r'^crawl-content', app.views.crawl_content, name="all_posts"),
    url(r'^start-crawl/(?P<crawl_target>\w+)$', app.views.start_crawl, name="crawl_content"),
    url(r'^login/$',
        django.contrib.auth.views.login,
        {
            'template_name': 'app/login.html',
            'authentication_form': app.forms.BootstrapAuthenticationForm,
            'extra_context':
            {
                'title': 'Log in',
                'year': datetime.now().year,
            }
        },
        name='login'),
    url(r'^logout$',
        django.contrib.auth.views.logout,
        {
            'next_page': '/',
        },
        name='logout'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # News Admin Extensions
    url(r'^admin/news', news.views.scraped_news, name='scraped_news'),
    url(r'^admin/create-news', news.views.create_news_article, name='create_news'),
    url(r'admin/save-news', news.views.save_news_article, name="save_news"),

    # Scraping APIs
    url(r'^admin/scraping/', include('scrapinghub.urls', namespace='scrapinghub', app_name='scrapinghub')),

    # Analytics Pages
    url(r'^admin/analytics_test', analyticshub.views.analytics_test, name='analyticshub'),

    # Admin Tools
    url(r'^admin_tools/', include('admin_tools.urls')),
    
    # ckeditor
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

]

admin.site.site_header = "Track_Fiend Administrator"
admin.site.site_title = "Track_Fiend admin"
