from django.contrib.admin import AdminSite

class TrackFiendAdminSite(AdminSite):
    site_header = 'Track_Fiend Dashboard'
    site_title = 'Track_Fiend admin'

admin_site = TrackFiendAdminSite(name='trackfiendadmin')

