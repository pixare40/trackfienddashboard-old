�}q (X   membersq}q(X
   _get_identq}q(X   kindqX   funcrefqX   valueq}qX	   func_nameq	X   dummy_thread.get_identq
suX   _activeq}q(hX   dataqh}qX   typeqX   __builtin__qX   dictq�qsuX   _dequeq}q(hX   typerefqh]qX   _collectionsqX   dequeq�qauX	   enumerateq}q(hX   functionqh}q(X   docqX�   Return a list of all Thread objects currently alive.

    The list includes daemonic threads, dummy thread objects created by
    current_thread(), and the main thread. It excludes terminated threads and
    threads that have not yet been started.qX   builtinq �X   locationq!M�K�q"X	   overloadsq#]q$}q%(X   argsq&)X   ret_typeq'hX   listq(]q)]q*(hX   tupleq+]q,(NNNe�q-hh+]q.(NNhX   NoneTypeq/�q0e�q1ea�q2uauuX   _VERBOSEq3}q4(hhh}q5hhX   boolq6�q7suX   _active_limbo_lockq8}q9(hhh}q:hX   dummy_threadq;X   LockTypeq<�q=suX
   _Conditionq>}q?(hhh}q@(X   mroqA]qB(X	   threadingqCh>�qDhCX   _VerboseqE�qFhFhX   objectqG�qHeX   basesqI]qJ(hFhFeh}qK(X	   notifyAllqL}qM(hhh}qN(hX�   Wake up all threads waiting on this condition.

        If the calling thread has not acquired the lock when this method
        is called, a RuntimeError is raised.qOh �h!M�K	�qPh#]qQ}qR(h&}qS(X   nameqTX   selfqUhhDu�qVh'NuauuX   _acquire_restoreqW}qX(hX   methodqYh}qZ(hNh �h!K�K	�q[h#]q\}q](h&}q^(hThUhhCX   _RLockq_�q`u}qa(hTX   count_ownerqbh]qc(hh+]qd(]qehX   intqf�qga]qh(hHh0ee�qihh+]qj(]qk(X   idlelib.rpcqlX   RPCProxyqm�qnhh+�qoX   idlelib.RemoteDebuggerqpX	   DictProxyqq�qrhpX	   CodeProxyqs�qthlX   RemoteProxyqu�qvhHhh(�qwhpX
   FrameProxyqx�qyh0hlX   MethodProxyqz�q{e]q|(h0hHee�q}eu�q~h'NuaX   boundq�uuX   __init__q�}q�(hhh}q�(hNh �h!MK	�q�h#]q�}q�(h&}q�(hThUhhDu}q�(hTX   lockq�h]q�(h`h=h0eX   default_valueq�X   Noneq�u}q�(hTX   verboseq�hh0h�h�u�q�h'NuauuX   waitq�}q�(hhh}q�(hX`  Wait until notified or until a timeout occurs.

        If the calling thread has not acquired the lock when this method is
        called, a RuntimeError is raised.

        This method releases the underlying lock, and then blocks until it is
        awakened by a notify() or notifyAll() call for the same condition
        variable in another thread, or until the optional timeout occurs. Once
        awakened or timed out, it re-acquires the lock and returns.

        When the timeout argument is present and not None, it should be a
        floating point number specifying a timeout for the operation in seconds
        (or fractions thereof).

        When the underlying lock is an RLock, it is not released using its
        release() method, since this may not actually unlock the lock when it
        was acquired multiple times recursively. Instead, an internal interface
        of the RLock class is used, which really unlocks it even when it has
        been recursively acquired several times. Another internal interface is
        then used to restore the recursion level when the lock is reacquired.q�h �h!M5K	�q�h#]q�}q�(h&}q�(hThUhhDu}q�(hTX   timeoutq�h]q�(hX   floatq��q�h0eh�h�u�q�h'NuauuX	   __enter__q�}q�(hhh}q�(hNh �h!MK	�q�h#]q�}q�(h&}q�(hThUhhDu�q�h']q�(hgh7euauuX   notifyq�}q�(hhh}q�(hXA  Wake up one or more threads waiting on this condition, if any.

        If the calling thread has not acquired the lock when this method is
        called, a RuntimeError is raised.

        This method wakes up at most n of the threads waiting for the condition
        variable; it is a no-op if no threads are waiting.q�h �h!MuK	�q�h#]q�}q�(h&}q�(hThUhhDu}q�(hTX   nq�hhgh�X   1q�u�q�h'NuauuX   __repr__q�}q�(hhh}q�(hNh �h!M#K	�q�h#]q�}q�(h&}q�(hThUhhDu�q�h'hX   strq��q�uauuX   _release_saveq�}q�(hhYh}q�(hNh �h!K�K	�q�h#]q�}q�(h&}q�(hThUhh`u�q�h']q�(hih}euah�uuX	   _is_ownedq�}q�(hhYh}q�(hNh �h!K�K	�q�h#]q�}q�(h&}q�(hThUhh`u�q�h'hguah�uuX   __exit__q�}q�(hhh}q�(hNh �h!M K	�q�h#]q�}q�(h&}q�(hThUhhDu}q�(X
   arg_formatq�X   *q�hTX   argsq�h]q�(hohh+]q�Na�q�eu�q�h'NuauuX
   notify_allq�}q�(hhh}q�(hX�   Wake up all threads waiting on this condition.

        If the calling thread has not acquired the lock when this method
        is called, a RuntimeError is raised.q�h �h!M�K	�q�h#]q�}q�(h&}q�(hThUhhDu�q�h'NuauuX   _Condition__lockq�}q�(hX   multipleq�h}q�h}q�(hhh}q�hh0su}q�(hhh}q�hh`su}q�(hhh}q�hh=su�q�suX   acquireq�}q�(hh�h}q�h}q�(hhYh}q�(hX�  Dummy implementation of acquire().

        For blocking calls, self.locked_status is automatically set to
        True and returned appropriately based on value of
        ``waitflag``.  If it is non-blocking, then the value is
        actually checked and not set if it is already acquired.  This
        is all done so that threading.Condition's assert statements
        aren't triggered and throw a little fit.q�h �h!K_K	�q�h#]q�}q�(h&}q�(hThUhh=u}q�(hTX   waitflagq�h]q�(h0h7hgeh�h�u�q�h'h7uah�uu}q�(hhYh}q�(hX�  Acquire a lock, blocking or non-blocking.

        When invoked without arguments: if this thread already owns the lock,
        increment the recursion level by one, and return immediately. Otherwise,
        if another thread owns the lock, block until the lock is unlocked. Once
        the lock is unlocked (not owned by any thread), then grab ownership, set
        the recursion level to one, and return. If more than one thread is
        blocked waiting until the lock is unlocked, only one at a time will be
        able to grab ownership of the lock. There is no return value in this
        case.

        When invoked with the blocking argument set to true, do the same thing
        as when called without arguments, and return true.

        When invoked with the blocking argument set to false, do not block. If a
        call without an argument would block, return false immediately;
        otherwise, do the same thing as when called without arguments, and
        return true.q h �h!K�K	�r  h#]r  }r  (h&}r  (hThUhh`u}r  (hTX   blockingr  hhgh�X   1r  u�r  h']r	  (hgh7euah�uu�r
  suX   releaser  }r  (hh�h}r  h}r  (hhYh}r  (hX   Release the dummy lock.r  h �h!KyK	�r  h#]r  }r  (h&}r  (hThUhh=u�r  h'h7uah�uu}r  (hhYh}r  (hXc  Release a lock, decrementing the recursion level.

        If after the decrement it is zero, reset the lock to unlocked (not owned
        by any thread), and if any other threads are blocked waiting for the
        lock to become unlocked, allow exactly one of them to proceed. If after
        the decrement the recursion level is still nonzero, the lock remains
        locked and owned by the calling thread.

        Only call this method when the calling thread owns the lock. A
        RuntimeError is raised if this method is called when the lock is
        unlocked.

        There is no return value.r  h �h!K�K	�r  h#]r  }r  (h&}r  (hThUhh`u�r  h'Nuah�uu�r  suX   _Condition__waitersr  }r   (hhh}r!  hhwsuX   _Verbose__verboser"  }r#  (hh�h}r$  h}r%  (hhh}r&  hh0su}r'  (hhh}r(  hh7su�r)  suuhXg   Condition variables allow one or more threads to wait until they are
       notified by another thread.r*  h �h!K�K�r+  uuX   currentThreadr,  }r-  (hhh}r.  (hX�   Return the current Thread object, corresponding to the caller's thread of control.

    If the caller's thread of control was not created through the threading
    module, a dummy thread object with limited functionality is returned.r/  h �h!MwK�r0  h#]r1  }r2  (h&)h']r3  (hCX   ProducerThreadr4  �r5  hCX   _MainThreadr6  �r7  hCX   ConsumerThreadr8  �r9  X   multiprocessing.dummyr:  X   DummyProcessr;  �r<  hCX   _DummyThreadr=  �r>  hCX   Threadr?  �r@  euauuX	   ConditionrA  }rB  (hhh}rC  (hXw  Factory function that returns a new condition variable object.

    A condition variable allows one or more threads to wait until they are
    notified by another thread.

    If the lock argument is given and not None, it must be a Lock or RLock
    object, and it is used as the underlying lock. Otherwise, a new RLock object
    is created and used as the underlying lock.rD  h �h!K�K�rE  h#]rF  }rG  (h&}rH  (h�h�hTh�h]rI  (h�hh+]rJ  h`a�rK  hh+]rL  h=a�rM  hoeu}rN  (h�X   **rO  hTX   kwargsrP  hhu�rQ  h'hDuauuX
   stack_sizerR  }rS  (hhh}rT  h	X   dummy_thread.stack_sizerU  suX   _TimerrV  }rW  (hhh}rX  (hA]rY  (hCjV  �rZ  j@  hFhFhHehI]r[  j@  ah}r\  (h�}r]  (hhh}r^  (hNh �h!M"K	�r_  h#]r`  }ra  (h&(}rb  (hThUhjZ  u}rc  (hTX   intervalrd  hNu}re  (hThhNu}rf  (hTh�hhwh�X   []rg  u}rh  (hTjP  hhh�X   {}ri  utrj  h'NuauuX   runrk  }rl  (hhh}rm  (hNh �h!M.K	�rn  h#]ro  }rp  (h&}rq  (hThUhjZ  u�rr  h'NuauuX   cancelrs  }rt  (hhh}ru  (hX(   Stop the timer if it hasn't finished yetrv  h �h!M*K	�rw  h#]rx  }ry  (h&}rz  (hThUhjZ  u�r{  h'NuauuX   intervalr|  }r}  (hhh}r~  hNsuX   functionr  }r�  (hhh}r�  hNsuX   argsr�  }r�  (hhh}r�  hhwsuX   kwargsr�  }r�  (hhh}r�  hhsuX   finishedr�  }r�  (hhh}r�  hhCX   _Eventr�  �r�  suuhX�   Call a function after a specified number of seconds:

            t = Timer(30.0, f, args=[], kwargs={})
            t.start()
            t.cancel()     # stop the timer's action if it's still waitingr�  h �h!MK�r�  uuX   __all__r�  }r�  (hhh}r�  hhh(]r�  (h�h�h�h�h�h�h�h�h�h�h�h�h�h�h�h�h�e�r�  suX   Timerr�  }r�  (hhh}r�  (hX�   Factory function to create a Timer object.

    Timers call a function after a specified number of seconds:

        t = Timer(30.0, f, args=[], kwargs={})
        t.start()
        t.cancel()     # stop the timer's action if it's still waitingr�  h �h!MK�r�  h#]r�  }r�  (h&}r�  (h�h�hTh�hhou}r�  (h�jO  hTjP  hhu�r�  h'jZ  uauuX   _format_excr�  }r�  (hhh}r�  h	X   traceback.format_excr�  suX   threadr�  }r�  (hX	   modulerefr�  hh;X    r�  �r�  uX
   setprofiler�  }r�  (hhh}r�  (hX�   Set a profile function for all threads started from the threading module.

    The func will be passed to sys.setprofile() for each thread, before its
    run() method is called.r�  h �h!KZK�r�  h#]r�  }r�  (h&}r�  (hTX   funcr�  hNu�r�  h'NuauuX
   _Semaphorer�  }r�  (hhh}r�  (hA]r�  (hCj�  �r�  hFhFhHehI]r�  (hFhFeh}r�  (h�}r�  (hhh}r�  (hX�  Acquire a semaphore, decrementing the internal counter by one.

        When invoked without arguments: if the internal counter is larger than
        zero on entry, decrement it by one and return immediately. If it is zero
        on entry, block, waiting until some other thread has called release() to
        make it larger than zero. This is done with proper interlocking so that
        if multiple acquire() calls are blocked, release() will wake exactly one
        of them up. The implementation may pick one at random, so the order in
        which blocked threads are awakened should not be relied on. There is no
        return value in this case.

        When invoked with blocking set to true, do the same thing as when called
        without arguments, and return true.

        When invoked with blocking set to false, do not block. If a call without
        an argument would block, return false immediately; otherwise, do the
        same thing as when called without arguments, and return true.r�  h �h!M�K	�r�  h#]r�  }r�  (h&}r�  (hThUhj�  u}r�  (hTj  hhgh�X   1r�  u�r�  h'h7uauuh�}r�  (hhh}r�  (hNh �h!M�K	�r�  h#]r�  }r�  (h&}r�  (hThUh]r�  (j�  hCX   _BoundedSemaphorer�  �r�  eu}r�  (hTX   valuer�  hhgh�X   1r�  u}r�  (hTh�hh0h�h�u�r�  h'Nuauuh�}r�  (hhh}r�  (hX�  Acquire a semaphore, decrementing the internal counter by one.

        When invoked without arguments: if the internal counter is larger than
        zero on entry, decrement it by one and return immediately. If it is zero
        on entry, block, waiting until some other thread has called release() to
        make it larger than zero. This is done with proper interlocking so that
        if multiple acquire() calls are blocked, release() will wake exactly one
        of them up. The implementation may pick one at random, so the order in
        which blocked threads are awakened should not be relied on. There is no
        return value in this case.

        When invoked with blocking set to true, do the same thing as when called
        without arguments, and return true.

        When invoked with blocking set to false, do not block. If a call without
        an argument would block, return false immediately; otherwise, do the
        same thing as when called without arguments, and return true.r�  h �h!M�K	�r�  h#]r�  }r�  (h&}r�  (hThUhj�  u}r�  (hTj  hhgh�X   1r�  u�r�  h'h7uauuh�}r�  (hhh}r�  (hNh �h!M�K	�r�  h#]r�  }r�  (h&(}r�  (hThUhj�  u}r�  (hTX   tr�  hNu}r�  (hTX   vr�  hNu}r�  (hTX   tbr�  hNutr�  h'Nuauuj  }r�  (hhh}r�  (hX�   Release a semaphore, incrementing the internal counter by one.

        When the counter is zero on entry and another thread is waiting for it
        to become larger than zero again, wake up that thread.r�  h �h!M�K	�r�  h#]r�  }r�  (h&}r�  (hThUhj�  u�r�  h'NuauuX   _Semaphore__condr�  }r�  (hhh}r�  hhDsuX   _Semaphore__valuer�  }r�  (hh�h}r�  h}r�  (hhh}r�  hhgsu}r�  (hhh}r�  hhgsu�r�  suj"  }r�  (hh�h}r�  h}r�  (hhh}r�  hh0su}r�  (hhh}r�  hh7su�r   suuhX  Semaphores manage a counter representing the number of release() calls
       minus the number of acquire() calls, plus an initial value. The acquire()
       method blocks if necessary until it can return without making the counter
       negative. If not given, value defaults to 1.r  h �h!M�K�r  uuj�  }r  (hhh}r  (hA]r  (j�  j�  hFhFhHehI]r  j�  ah}r  (j  }r  (hhh}r	  (hX,  Release a semaphore, incrementing the internal counter by one.

        When the counter is zero on entry and another thread is waiting for it
        to become larger than zero again, wake up that thread.

        If the number of releases exceeds the number of acquires,
        raise a ValueError.r
  h �h!MK	�r  h#]r  }r  (h&}r  (hThUhj�  u�r  h'Nuauuh�}r  (hhh}r  (hNh �h!M	K	�r  h#]r  }r  (h&}r  (hThUhj�  u}r  (hTj�  hhgh�X   1r  u}r  (hTh�hh0h�h�u�r  h'NuauuX   _initial_valuer  }r  (hhh}r  hhgsuX   _Semaphore__condr  }r  (hhh}r  hhDsuX   _Semaphore__valuer   }r!  (hhh}r"  hhgsuj"  }r#  (hh�h}r$  h}r%  (hhh}r&  hh0su}r'  (hhh}r(  hh7su�r)  suuhX�   A bounded semaphore checks to make sure its current value doesn't exceed
       its initial value. If it does, ValueError is raised. In most situations
       semaphores are used to guard resources with limited capacity.r*  h �h!MK�r+  uuhE}r,  (hh�h}r-  h}r.  (hhh}r/  (hA]r0  (hFhHehI]r1  hHah}r2  (h�}r3  (hhh}r4  (hNh �h!K;K�r5  h#]r6  }r7  (h&}r8  (hThUh]r9  (j�  hFj�  hCX   BoundedQueuer:  �r;  hDh`j�  eu}r<  (hTh�h]r=  (h0h7eh�h�u�r>  h'NuauuX   _noter?  }r@  (hhh}rA  (hNh �h!K@K�rB  h#]rC  }rD  (h&}rE  (hThUh]rF  (hFj5  h`j;  j7  hDj9  j�  j<  j@  eu}rG  (hTX   formatrH  hh�u}rI  (h�h�hTh�h]rJ  (hh+]rK  ]rL  h�aa�rM  hh+]rN  ]rO  (j�  hgea�rP  hh+]rQ  hDa�rR  jK  hh+]rS  ]rT  (hgh�ea�rU  hohh+]rV  h�a�rW  hh+]rX  j7  a�rY  hh+]rZ  ]r[  (hghHea�r\  h�eu�r]  h'Nuauuj"  }r^  (hh�h}r_  h}r`  (hhh}ra  hh0su}rb  (hhh}rc  hh7su�rd  suuhNh �h!K9K�re  uu}rf  (hhh}rg  (hA]rh  (hFhHehI]ri  hHah}rj  (h�}rk  (hhh}rl  (hNh �h!KPK�rm  h#]rn  }ro  (h&}rp  (hThUh]rq  (j�  j�  hFj;  hDh`j�  eu}rr  (hTh�hh0h�h�u�rs  h'Nuauuj?  }rt  (hhh}ru  (hNh �h!KRK�rv  h#]rw  }rx  (h&}ry  (hThUhhFu}rz  (h�h�hTh�hhou�r{  h'NuauuuhNh �h!KOK�r|  uu�r}  suX	   _shutdownr~  }r  (hhYh}r�  (hNh �h!MCK	�r�  h#]r�  }r�  (h&}r�  (hThUhj7  u�r�  h'Nuah�uuX   _pickSomeNonDaemonThreadr�  }r�  (hhh}r�  (hNh �h!MPK�r�  h#]r�  }r�  (h&)h']r�  (j5  j>  h0j9  j7  j@  euauuX   settracer�  }r�  (hhh}r�  (hX�   Set a trace function for all threads started from the threading module.

    The func will be passed to sys.settrace() for each thread, before its run()
    method is called.r�  h �h!KdK�r�  h#]r�  }r�  (h&}r�  (hTj�  h]r�  h0au�r�  h'Nuauuj?  }r�  (hhh}r�  (hA]r�  (j@  hFhFhHehI]r�  (hFhFeh}r�  (X   namer�  }r�  (hX   propertyr�  h}r�  (hX�   A string used for identification purposes only.

        It has no semantics. Multiple threads may be given the same name. The
        initial name is set by the constructor.r�  hh�h!M�K	�r�  uuX   _Thread__exc_clearr�  }r�  (hhh}r�  h	X   sys.exc_clearr�  suX   _set_daemonr�  }r�  (hhh}r�  (hNh �h!M�K	�r�  h#]r�  }r�  (h&}r�  (hThUhj@  u�r�  h'h7uauuX   startr�  }r�  (hhh}r�  (hX#  Start the thread's activity.

        It must be called at most once per thread object. It arranges for the
        object's run() method to be invoked in a separate thread of control.

        This method will raise a RuntimeError if called more than once on the
        same thread object.r�  h �h!M�K	�r�  h#]r�  }r�  (h&}r�  (hThUh]r�  (j@  j<  eu�r�  h'NuauuX   _Thread__bootstrap_innerr�  }r�  (hhh}r�  (hNh �h!MK	�r�  h#]r�  }r�  (h&}r�  (hThUh]r�  (j<  j9  j5  j@  eu�r�  h'NuauuX
   _set_identr�  }r�  (hhh}r�  (hNh �h!MK	�r�  h#]r�  }r�  (h&}r�  (hThUh]r�  (j5  j7  j<  j9  j>  j@  eu�r�  h'NuauuX   _Thread__deleter�  }r�  (hhh}r�  (hXA   Remove current thread from the dict of currently running threads.r�  h �h!McK	�r�  h#]r�  }r�  (h&}r�  (hThUh]r�  (j@  j7  eu�r�  h'NuauuX   _Thread__stopr�  }r�  (hhh}r�  (hNh �h!MYK	�r�  h#]r�  }r�  (h&}r�  (hThUh]r�  (j5  j7  j<  j9  j>  j@  eu�r�  h'NuauuX   joinr�  }r�  (hhh}r�  (hX  Wait until the thread terminates.

        This blocks the calling thread until the thread whose join() method is
        called terminates -- either normally or through an unhandled exception
        or until the optional timeout occurs.

        When the timeout argument is present and not None, it should be a
        floating point number specifying a timeout for the operation in seconds
        (or fractions thereof). As join() always returns None, you must call
        isAlive() after join() to decide whether a timeout happened -- if the
        thread is still alive, the join() call timed out.

        When the timeout argument is not present or None, the operation will
        block until the thread terminates.

        A thread can be join()ed many times.

        join() raises a RuntimeError if an attempt is made to join the current
        thread as that would cause a deadlock. It is also an error to join() a
        thread before it has been started and attempts to do so raises the same
        exception.r�  h �h!M�K	�r�  h#]r�  }r�  (h&}r�  (hThUh]r�  (j7  j9  j<  j5  j@  eu}r�  (hTh�hh0h�h�u�r�  h'Nuauuh�}r�  (hhh}r�  (hXA  This constructor should always be called with keyword arguments. Arguments are:

        *group* should be None; reserved for future extension when a ThreadGroup
        class is implemented.

        *target* is the callable object to be invoked by the run()
        method. Defaults to None, meaning nothing is called.

        *name* is the thread name. By default, a unique name is constructed of
        the form "Thread-N" where N is a small decimal number.

        *args* is the argument tuple for the target invocation. Defaults to ().

        *kwargs* is a dictionary of keyword arguments for the target
        invocation. Defaults to {}.

        If a subclass overrides the constructor, it must make sure to invoke
        the base class constructor (Thread.__init__()) before doing anything
        else to the thread.r�  h �h!M�K	�r�  h#]r�  }r�  (h&(}r�  (hThUhj@  u}r�  (hTX   groupr�  hh0h�h�u}r�  (hTX   targetr�  hh0h�h�u}r�  (hTX   namer�  hh0h�h�u}r�  (hTh�hhoh�X   ()r�  u}r�  (hTjP  h]r�  (hh0eh�h�u}r�  (hTh�hh0h�h�utr�  h'NuauuX   _Thread__exc_infor�  }r�  (hhh}r�  h	X   sys.exc_infor�  suX   _reset_internal_locksr�  }r   (hhh}r  (hNh �h!M�K	�r  h#]r  }r  (h&}r  (hThUh]r  (j7  j>  j@  eu�r  h'NuauuX   identr  }r	  (hj�  h}r
  (hX1  Thread identifier of this thread or None if it has not been started.

        This is a nonzero integer. See the thread.get_ident() function. Thread
        identifiers may be recycled when a thread exits and another thread is
        created. The identifier is available even after the thread has exited.r  h]r  (hHh0eh!M�K	�r  uuX   setNamer  }r  (hhh}r  (hNh �h!MK	�r  h#]r  }r  (h&}r  (hThUhj@  u}r  (hTj�  hNu�r  h'NuauuX   isDaemonr  }r  (hhh}r  (hNh �h!M�K	�r  h#]r  }r  (h&}r  (hThUhj@  u�r  h'h7uauuX   daemonr  }r   (hh�h}r!  h}r"  (hhh}r#  hh7su}r$  (hhh}r%  hh7su�r&  suX   _Thread__initializedr'  }r(  (hhh}r)  hh7suX   is_aliver*  }r+  (hhh}r,  (hX�   Return whether the thread is alive.

        This method returns True just before the run() method starts until just
        after the run() method terminates. The module function enumerate()
        returns a list of all alive threads.r-  h �h!M�K	�r.  h#]r/  }r0  (h&}r1  (hThUh]r2  (j7  j>  j<  j9  j5  j@  eu�r3  h'h7uauujk  }r4  (hhh}r5  (hXN  Method representing the thread's activity.

        You may override this method in a subclass. The standard run() method
        invokes the callable object passed to the object's constructor as the
        target argument, if any, with sequential and keyword arguments taken
        from the args and kwargs arguments, respectively.r6  h �h!M�K	�r7  h#]r8  }r9  (h&}r:  (hThUh]r;  (j@  j<  eu�r<  h'NuauuX   _Thread__bootstrapr=  }r>  (hhh}r?  (hNh �h!M�K	�r@  h#]rA  }rB  (h&}rC  (hThUh]rD  (j<  j9  j5  j@  eu�rE  h'NuauuX   getNamerF  }rG  (hhh}rH  (hNh �h!MK	�rI  h#]rJ  }rK  (h&}rL  (hThUh]rM  (j>  j7  j@  j<  j5  j9  eu�rN  h'h�uauuX   _blockrO  }rP  (hj�  h}rQ  (hNhhDh!M�K	�rR  uuh�}rS  (hhh}rT  (hNh �h!M�K	�rU  h#]rV  }rW  (h&}rX  (hThUhj@  u�rY  h'h�uauuX   isAliverZ  }r[  (hhh}r\  (hX�   Return whether the thread is alive.

        This method returns True just before the run() method starts until just
        after the run() method terminates. The module function enumerate()
        returns a list of all alive threads.r]  h �h!M�K	�r^  h#]r_  }r`  (h&}ra  (hThUh]rb  (j7  j>  j<  j9  j5  j@  eu�rc  h'h7uauuX	   setDaemonrd  }re  (hhh}rf  (hNh �h!MK	�rg  h#]rh  }ri  (h&}rj  (hThUhj@  u}rk  (hTX   daemonicrl  hh7u�rm  h'NuauuX   _Thread__targetrn  }ro  (hhh}rp  hh0suX   _Thread__namerq  }rr  (hhh}rs  hh�suX   _Thread__argsrt  }ru  (hhh}rv  hhosuX   _Thread__kwargsrw  }rx  (hh�h}ry  h}rz  (hhh}r{  hh0su}r|  (hhh}r}  hhsu�r~  suX   _Thread__daemonicr  }r�  (hh�h}r�  h}r�  (hhh}r�  hh7su}r�  (hhh}r�  hh7su�r�  suX   _Thread__identr�  }r�  (hh�h}r�  h}r�  (hhh}r�  hh0su}r�  (hhh}r�  hhHsu�r�  suX   _Thread__startedr�  }r�  (hhh}r�  hj�  suX   _Thread__stoppedr�  }r�  (hhh}r�  hh7suX   _Thread__blockr�  }r�  (hhh}r�  hhDsuX   _Thread__stderrr�  }r�  (hhh}r�  hhX   filer�  �r�  suj"  }r�  (hh�h}r�  h}r�  (hhh}r�  hh0su}r�  (hhh}r�  hh7su�r�  suX   _stater�  }r�  (hh�h}r�  h}r�  (hhh}r�  hhgsu}r�  (hhh}r�  hhgsu}r�  (hhh}r�  hhgsu�r�  suX	   _childrenr�  }r�  (hhh}r�  hX   weakrefr�  X   WeakKeyDictionaryr�  �r�  suX   __decimal_context__r�  }r�  (hhh}r�  hX   decimalr�  X   Contextr�  �r�  suuhXk   A class that represents a thread of control.

    This class can be safely subclassed in a limited fashion.r�  h �h!MwK�r�  uuX   localr�  }r�  (hhh]r�  X   _threading_localr�  X   localr�  �r�  auX   _testr�  }r�  (hhh}r�  (hNh �h!M�K�r�  h#]r�  }r�  (h&)h'NuauuX   RLockr�  }r�  (hhh}r�  (hX,  Factory function that returns a new reentrant lock.

    A reentrant lock must be released by the thread that acquired it. Once a
    thread has acquired a reentrant lock, the same thread may acquire it again
    without blocking; the thread must release it once for each time it has
    acquired it.r�  h �h!KrK�r�  h#]r�  }r�  (h&}r�  (h�h�hTh�h]r�  (h�hoeu}r�  (h�jO  hTjP  hhu�r�  h'h`uauuX   _start_new_threadr�  }r�  (hhh}r�  h	X   dummy_thread.start_new_threadr�  suX   __file__r�  }r�  (hh�h}r�  h(}r�  (hhh}r�  hh0su}r�  (hhh}r�  hh�su}r�  (hhh}r�  hX   genericpathr�  X   _unicoder�  �r�  su}r�  (hhh}r�  hhX   unicoder�  �r�  su}r�  (hhh}r�  hhh(]r�  (]r�  (hHh0e]r�  (h�hHhwhgj�  j�  h0e]r�  (h�hHhwhgj�  j�  h0ee�r�  su}r�  (hhh}r�  hhHsu}r�  (hhh}r�  hj�  su}r�  (hhh}r�  hhwsu}r�  (hhh}r�  hhh(]r�  ]r�  (j�  hh(�r�  ea�r�  su}r�  (hhh}r�  hhgsu}r�  (hhh}r�  hh�su}r�  (hhh}r�  hh0su}r   (hhh}r  hh�su}r  (hhh}r  hh�su}r  (hhh}r  hhh(]r  hh(�r  a�r  sutr	  suX   _counterr
  }r  (hhYhhYuX   _limbor  }r  (hhh}r  hhsuX   _sleepr  }r  (hhh}r  h	X
   time.sleepr  suX   _profile_hookr  }r  (hhh}r  hh0suX   Lockr  }r  (hhh}r  h	X   dummy_thread.allocate_lockr  suX   warningsr  }r  (hj�  hX   warningsr  j�  �r  uX   _sysr  }r  (hj�  hX   sysr   j�  �r!  uj�  }r"  (hhh}r#  (hA]r$  (j�  hFhFhHehI]r%  (hFhFeh}r&  (h�}r'  (hhh}r(  (hNh �h!M1K	�r)  h#]r*  }r+  (h&}r,  (hThUhj�  u}r-  (hTh�hh0h�h�u�r.  h'NuauuX   isSetr/  }r0  (hhh}r1  (hX5   Return true if and only if the internal flag is true.r2  h �h!M:K	�r3  h#]r4  }r5  (h&}r6  (hThUhj�  u�r7  h'h7uauuX   is_setr8  }r9  (hhh}r:  (hj2  h �h!M:K	�r;  h#]r<  }r=  (h&}r>  (hThUhj�  u�r?  h'h7uauuX   setr@  }rA  (hhh}rB  (hX�   Set the internal flag to true.

        All threads waiting for the flag to become true are awakened. Threads
        that call wait() once the flag is true will not block at all.rC  h �h!M@K	�rD  h#]rE  }rF  (h&}rG  (hThUhj�  u�rH  h'Nuauuh�}rI  (hhh}rJ  (hX>  Block until the internal flag is true.

        If the internal flag is true on entry, return immediately. Otherwise,
        block until another thread calls set() to set the flag to true, or until
        the optional timeout occurs.

        When the timeout argument is present and not None, it should be a
        floating point number specifying a timeout for the operation in seconds
        (or fractions thereof).

        This method returns the internal flag on exit, so it will always return
        True except if a timeout is given and the operation times out.rK  h �h!MUK	�rL  h#]rM  }rN  (h&}rO  (hThUhj�  u}rP  (hTh�hh0h�h�u�rQ  h'h7uauuj�  }rR  (hhh}rS  (hNh �h!M6K	�rT  h#]rU  }rV  (h&}rW  (hThUhj�  u�rX  h'NuauuX   clearrY  }rZ  (hhh}r[  (hX�   Reset the internal flag to false.

        Subsequently, threads calling wait() will block until set() is called to
        set the internal flag to true again.r\  h �h!MKK	�r]  h#]r^  }r_  (h&}r`  (hThUhj�  u�ra  h'NuauuX   _Event__condrb  }rc  (hhh}rd  hhDsuX   _Event__flagre  }rf  (hhh}rg  hh7suj"  }rh  (hh�h}ri  h}rj  (hhh}rk  hh0su}rl  (hhh}rm  hh7su�rn  suuhX�   A factory function that returns a new event object. An event manages a
       flag that can be set to true with the set() method and reset to false
       with the clear() method. The wait() method blocks until the flag is true.ro  h �h!M(K�rp  uuX   active_countrq  }rr  (hhh}rs  (hX�   Return the number of Thread objects currently alive.

    The returned count is equal to the length of the list returned by
    enumerate().rt  h �h!M�K�ru  h#]rv  }rw  (h&)h'hguauuX   _after_forkrx  }ry  (hhh}rz  (hNh �h!M�K�r{  h#]r|  }r}  (h&)h'Nuauuh_}r~  (hhh}r  (hA]r�  (h`hFhFhHehI]r�  (hFhFeh}r�  (h�}r�  (hhh}r�  (hX�  Acquire a lock, blocking or non-blocking.

        When invoked without arguments: if this thread already owns the lock,
        increment the recursion level by one, and return immediately. Otherwise,
        if another thread owns the lock, block until the lock is unlocked. Once
        the lock is unlocked (not owned by any thread), then grab ownership, set
        the recursion level to one, and return. If more than one thread is
        blocked waiting until the lock is unlocked, only one at a time will be
        able to grab ownership of the lock. There is no return value in this
        case.

        When invoked with the blocking argument set to true, do the same thing
        as when called without arguments, and return true.

        When invoked with the blocking argument set to false, do not block. If a
        call without an argument would block, return false immediately;
        otherwise, do the same thing as when called without arguments, and
        return true.r�  h �h!K�K	�r�  h#]r�  }r�  (h&}r�  (hThUhh`u}r�  (hTj  hhgh�X   1r�  u�r�  h']r�  (hgh7euauuh�}r�  (hhh}r�  (hNh �h!K�K	�r�  h#]r�  }r�  (h&}r�  (hThUhh`u}r�  (hTh�hh0h�h�u�r�  h'Nuauuh�}r�  (hhh}r�  (hNh �h!K�K	�r�  h#]r�  }r�  (h&}r�  (hThUhh`u�r�  h'h�uauuh�}r�  (hhh}r�  (hNh �h!K�K	�r�  h#]r�  }r�  (h&}r�  (hThUhh`u�r�  h'hguauuh�}r�  (hhh}r�  (hX�  Acquire a lock, blocking or non-blocking.

        When invoked without arguments: if this thread already owns the lock,
        increment the recursion level by one, and return immediately. Otherwise,
        if another thread owns the lock, block until the lock is unlocked. Once
        the lock is unlocked (not owned by any thread), then grab ownership, set
        the recursion level to one, and return. If more than one thread is
        blocked waiting until the lock is unlocked, only one at a time will be
        able to grab ownership of the lock. There is no return value in this
        case.

        When invoked with the blocking argument set to true, do the same thing
        as when called without arguments, and return true.

        When invoked with the blocking argument set to false, do not block. If a
        call without an argument would block, return false immediately;
        otherwise, do the same thing as when called without arguments, and
        return true.r�  h �h!K�K	�r�  h#]r�  }r�  (h&}r�  (hThUhh`u}r�  (hTj  hhgh�X   1r�  u�r�  h']r�  (hgh7euauuh�}r�  (hhh}r�  (hNh �h!K�K	�r�  h#]r�  }r�  (h&(}r�  (hThUhh`u}r�  (hTj�  hNu}r�  (hTj�  hNu}r�  (hTj�  hNutr�  h'Nuauuj  }r�  (hhh}r�  (hXc  Release a lock, decrementing the recursion level.

        If after the decrement it is zero, reset the lock to unlocked (not owned
        by any thread), and if any other threads are blocked waiting for the
        lock to become unlocked, allow exactly one of them to proceed. If after
        the decrement the recursion level is still nonzero, the lock remains
        locked and owned by the calling thread.

        Only call this method when the calling thread owns the lock. A
        RuntimeError is raised if this method is called when the lock is
        unlocked.

        There is no return value.r�  h �h!K�K	�r�  h#]r�  }r�  (h&}r�  (hThUhh`u�r�  h'Nuauuh�}r�  (hhh}r�  (hNh �h!K�K	�r�  h#]r�  }r�  (h&}r�  (hThUhh`u�r�  h']r�  (hih}euauuhW}r�  (hhh}r�  (hNh �h!K�K	�r�  h#]r�  }r�  (h&}r�  (hThUhh`u}r�  (hThbh]r�  (hih}eu�r�  h'NuauuX   _RLock__blockr�  }r�  (hhh}r�  hh=suX   _RLock__ownerr�  }r�  (hh�h}r�  h}r�  (hhh}r�  hhHsu}r�  (hhh}r�  hh0su�r�  suX   _RLock__countr�  }r�  (hh�h}r�  h}r�  (hhh}r�  hhgsu}r�  (hhh}r�  hhgsu}r�  (hhh}r�  hhgsu�r�  suj"  }r�  (hh�h}r�  h}r�  (hhh}r�  hh0su}r�  (hhh}r�  hh7su�r�  suuhX�   A reentrant lock must be released by the thread that acquired it. Once a
       thread has acquired a reentrant lock, the same thread may acquire it
       again without blocking; the thread must release it once for each time it
       has acquired it.r�  h �h!K}K�r�  uuX   _newnamer�  }r�  (hhh}r�  (hNh �h!MlK�r�  h#]r�  }r�  (h&}r�  (hTX   templater�  hh�h�X   'Thread-%d'r�  u�r�  h'h�uauuX   current_threadr�  }r�  (hhh}r�  (hX�   Return the current Thread object, corresponding to the caller's thread of control.

    If the caller's thread of control was not created through the threading
    module, a dummy thread object with limited functionality is returned.r�  h �h!MwK�r�  h#]r�  }r   (h&)h']r  (j5  j7  j9  j<  j>  j@  euauuX	   Semaphorer  }r  (hhh}r  (hXI  A factory function that returns a new semaphore.

    Semaphores manage a counter representing the number of release() calls minus
    the number of acquire() calls, plus an initial value. The acquire() method
    blocks if necessary until it can return without making the counter
    negative. If not given, value defaults to 1.r  h �h!M�K�r  h#]r  }r  (h&}r	  (h�h�hTh�hhou}r
  (h�jO  hTjP  hhu�r  h'j�  uauuX   activeCountr  }r  (hhh}r  (hX�   Return the number of Thread objects currently alive.

    The returned count is equal to the length of the list returned by
    enumerate().r  h �h!M�K�r  h#]r  }r  (h&)h'hguauuj=  }r  (hhh}r  (hA]r  (j>  j@  hFhFhHehI]r  j@  ah}r  (h�}r  (hhh}r  (hNh �h!MaK	�r  h#]r  }r  (h&}r  (hThUhj>  u�r  h'Nuauuj�  }r  (hhh}r   (hNh �h!MnK	�r!  h#]r"  }r#  (h&}r$  (hThUhj>  u�r%  h'h7uauuj�  }r&  (hhh}r'  (hNh �h!MqK	�r(  h#]r)  }r*  (h&}r+  (hThUhj>  u}r,  (hTh�hh0h�h�u�r-  h'NuauuX   _Thread__blockr.  }r/  (hhh}r0  hNsuj�  }r1  (hhh}r2  hj�  suj�  }r3  (hhh}r4  hj�  suX   _Thread__identr5  }r6  (hhh}r7  hhHsuX   _Thread__stoppedr8  }r9  (hhh}r:  hh7suuhNh �h!M_K�r;  uuX   Eventr<  }r=  (hhh}r>  (hX�   A factory function that returns a new event.

    Events manage a flag that can be set to true with the set() method and reset
    to false with the clear() method. The wait() method blocks until the flag is
    true.r?  h �h!MK�r@  h#]rA  }rB  (h&}rC  (h�h�hTh�h]rD  (h�hoeu}rE  (h�jO  hTjP  hhu�rF  h'j�  uauuX   _trace_hookrG  }rH  (hh�h}rI  h(}rJ  (hhh}rK  hh0su}rL  (hhYh}rM  (hX�   Handler for call events.

        If the code block being entered is to be ignored, returns `None',
        else returns self.localtrace.rN  h �h!MNK	�rO  h#]rP  }rQ  (h&(}rR  (hThUhX   tracerS  X   TracerT  �rU  u}rV  (hTX   framerW  hNu}rX  (hTX   whyrY  hNu}rZ  (hTX   argr[  hNutr\  h']r]  h0auah�uu}r^  (hhYh}r_  (hXb   Handler for call events.

        Adds information about who called who to the self._callers dict.r`  h �h!M:K	�ra  h#]rb  }rc  (h&(}rd  (hThUhjU  u}re  (hTjW  hNu}rf  (hTjY  hNu}rg  (hTj[  hNutrh  h'Nuah�uu}ri  (hhYh}rj  (hXf   Handler for call events.

        Adds (filename, modulename, funcname) to the self._calledfuncs dict.rk  h �h!MEK	�rl  h#]rm  }rn  (h&(}ro  (hThUhjU  u}rp  (hTjW  hNu}rq  (hTjY  hNu}rr  (hTj[  hNutrs  h'Nuah�uutrt  suX   _allocate_lockru  }rv  (hhh}rw  h	j  suj6  }rx  (hhh}ry  (hA]rz  (j7  j@  hFhFhHehI]r{  j@  ah}r|  (h�}r}  (hhh}r~  (hNh �h!M9K	�r  h#]r�  }r�  (h&}r�  (hThUhj7  u�r�  h'Nuauuj�  }r�  (hhh}r�  (hNh �h!M@K	�r�  h#]r�  }r�  (h&}r�  (hThUhj7  u�r�  h'h7uauuX	   _exitfuncr�  }r�  (hhh}r�  (hNh �h!MCK	�r�  h#]r�  }r�  (h&}r�  (hThUhj7  u�r�  h'Nuauuj5  }r�  (hhh}r�  hhHsuj8  }r�  (hhh}r�  hh7suj�  }r�  (hhh}r�  hj�  suj�  }r�  (hhh}r�  hj�  suuhNh �h!M7K�r�  uuX   ThreadErrorr�  }r�  (hhh]r�  h;X   errorr�  �r�  auX   _countr�  }r�  (hhh]r�  X	   itertoolsr�  X   countr�  �r�  auX
   _enumerater�  }r�  (hhh}r�  (hNh �h!M�K�r�  h#]r�  }r�  (h&)h'h2uauuX   BoundedSemaphorer�  }r�  (hhh}r�  (hX�  A factory function that returns a new bounded semaphore.

    A bounded semaphore checks to make sure its current value doesn't exceed its
    initial value. If it does, ValueError is raised. In most situations
    semaphores are used to guard resources with limited capacity.

    If the semaphore is released too many times it's a sign of a bug. If not
    given, value defaults to 1.

    Like regular semaphores, bounded semaphores manage a counter representing
    the number of release() calls minus the number of acquire() calls, plus an
    initial value. The acquire() method blocks if necessary until it can return
    without making the counter negative. If not given, value defaults to 1.r�  h �h!M�K�r�  h#]r�  }r�  (h&}r�  (h�h�hTh�hhou}r�  (h�jO  hTjP  hhu�r�  h'j�  uauuX   _timer�  }r�  (hhh}r�  h	X	   time.timer�  suuhX;   Thread module emulating a subset of Java's threading model.r�  X   childrenr�  ]r�  X   filenamer�  XA   c:\users\kabaj\appdata\local\continuum\anaconda2\lib\threading.pyr�  u.