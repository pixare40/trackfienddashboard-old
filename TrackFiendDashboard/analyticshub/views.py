import pickle
from analyticshub.vectorizer import vect
import re
import os

from django.shortcuts import render
from .forms import AnalyticsTest

def analytics_test(request):
    test_results = dict()
    label = {0:'negative', 1:'positive'}
    if request.method == 'POST':
        form = AnalyticsTest(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            text = form_data['test_text']
            text_array = [text]
            X = vect.transform(text_array)
            
            classifier = pickle.load(open(os.path.abspath('./classifiers/classifier.pkl'), 'rb'))

            sentiment_label = label[classifier.predict(X)[0]]
            sentiment_probability = classifier.predict_proba(X).max()*100

            test_results = dict()
            test_results['prediction'] = sentiment_label
            test_results['probability'] = sentiment_probability

    else:
        form = AnalyticsTest()

    return render(request, 'admin/analyticshub/analytics_test.html', { 'form': form, 'test_results': test_results })