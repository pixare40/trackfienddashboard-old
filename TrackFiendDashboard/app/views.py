"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from django.shortcuts import redirect
from django.template import RequestContext
from datetime import datetime
from TrackFiendDashboard import settings
from tasks import initiate_twitter_crawl
from tasks import initiate_artist_twitter_news
import pymongo

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )

def all_posts(request, page):
    """ Render All Posts """
    assert isinstance(request, HttpRequest)
    mongo_uri = settings.MONGO_URI
    mongo_database = settings.MONGO_DATABASE
    news_collection_name = "news"

    client = pymongo.MongoClient(mongo_uri)
    db = client[mongo_database]
    per_page = 10
    skip_value = (int(page) - 1) * per_page
    scraped_news = db[news_collection_name].find().sort('_id', pymongo.DESCENDING).skip(skip_value).limit(per_page)
    total_count = db[news_collection_name].find().count()
    pages = total_count / per_page
    client.close()

    return render(
        request,
        'app/posts/all_posts.html',
        {'news':scraped_news, 'total': range(pages)})

def crawl_content(request):
    return render(request, 'app/crawl/crawl_content.html')

def start_crawl(request, crawl_target):
    print crawl_target
    if crawl_target == "artist_crawl":
        print "crawling artists"
        get_top_artist_news(request)
    else:
        print "crawling everything else"
        initiate_twitter_crawl.delay()
    return redirect("http://127.0.0.1:5555/dashboard")

def get_top_artist_news(request):
    initiate_artist_twitter_news.delay()
    return redirect("http://127.0.0.1:5555/dashboard")

