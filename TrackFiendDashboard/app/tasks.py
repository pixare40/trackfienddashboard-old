from multiprocessing import Process
from celery import task
from twython import Twython
import pymongo
from TrackFiendDashboard import settings
import time
import datetime
import json
import urllib
import django
django.setup()
APP_KEY = 'aHFKl04PpjuW9xqgUyPra1FiH'
APP_SECRET = 'tnW0IjcFfVnvW9EEYmtQXbJFH4J0Hvv7IFvXGtDmK5DzVn2RMm'

@task()
def intiateHotNewHipHopArtistsCrawl():
    p = Process(target=startCrawlingHotnewHiphop)
    p.start()
    p.join()

def startCrawlingHotnewHiphop():
    from artistbots.spiders import runner
    runner.run()

@task()
def initiate_twitter_crawl():
    p = Process(target=start_twitter_crawl)
    p.start()
    p.join()

@task()
def initiate_artist_twitter_news():
    p = Process(target=get_top_twitter_artist_news)
    p.start()
    p.join()

def get_top_twitter_artist_news():
    from app.models import Account, Artist
    mongo_uri = settings.MONGO_URI
    mongo_database = settings.MONGO_DATABASE
    news_collection_name = "news"
    socials_collections_name = "socials"
    client = pymongo.MongoClient(mongo_uri)
    db = client[mongo_database]

    twitter_socials = db[socials_collections_name].find({'social_network': 'twitter'}, {'accounts': 1})
    # basically saying gimme accounts field which is an array. this might be ugly af one of you brilliant devs will show me how to code python well
    print twitter_socials
    twitter = Twython(APP_KEY, APP_SECRET, oauth_version=2)
    ACCESS_TOKEN = twitter.obtain_access_token()
    twitter = Twython(APP_KEY, access_token=ACCESS_TOKEN)
    all_account_artists = Account.objects.values_list('artists__name', flat=True).distinct()
    
    for artist in all_account_artists:
        if artist is None:
            continue
        results = twitter.search(q=artist, count=2)
        for result in results['statuses']:
            print results
            if artist not in result['text']:
                continue
            if db[news_collection_name].find({'title': result['text']}).count() > 0:
                continue
            else:
                collect_twitter_news(artist, db, news_collection_name, result)

def start_twitter_crawl():
    from app.models import Account, Artist
    mongo_uri = settings.MONGO_URI
    mongo_database = settings.MONGO_DATABASE
    news_collection_name = "news"
    socials_collections_name = "socials"
    client = pymongo.MongoClient(mongo_uri)
    db = client[mongo_database]

    twitter_socials = db[socials_collections_name].find({'social_network': 'twitter'}, {'accounts':1})
    # basically saying gimme accounts field which is an array. this might be ugly af one of you brilliant devs will show me how to code python well
    print twitter_socials
    twitter = Twython(APP_KEY, APP_SECRET, oauth_version=2)
    ACCESS_TOKEN = twitter.obtain_access_token()
    twitter = Twython(APP_KEY, access_token=ACCESS_TOKEN)
    all_account_artists = Account.objects.values_list('artists__name', flat=True).distinct()
    for accounts in twitter_socials:
        for account in accounts['accounts']:
            if account is None:
                continue
            print account
            results = twitter.get_user_timeline(user_id=account['user_id'], screen_name=account['screen_name'], count=5)
            print results
            for result in results:
                for artist in all_account_artists:
                    print artist
                    if artist is None:
                        continue
                    if unicode.lower(artist) not in unicode.lower(result['text']):
                        continue
                    if db[news_collection_name].find({'title': result['text']}).count() > 0:
                        if db[news_collection_name].find({'title': result['text'], 'tags': {'$in': unicode.lower(artist)}}).count() > 0:
                            continue
                        db[news_collection_name].update({'title': result['text']}, {'tags': {'$push' : unicode.lower(artist)}}, upsert=False)
                        continue
                    else:
                        collect_twitter_news(artist, db, news_collection_name, result)


def collect_twitter_news(artist, db, news_collection_name, result):
    # type: (artist_name, db, news_collection_name, result_object) -> object
    db[news_collection_name].insert({'tags': [unicode.lower(artist)],
                                     'title': result['text'],
                                     'tweet_text': result['text'],
                                     'url_to_story': get_url_to_story(result),
                                     'media_url': get_media_url(result),
                                     'media_url_https': get_media_https_url(result),
                                     'media_type': get_media_type(result),
                                     'media_sizes': get_media_sizes(result),
                                     'date_created': time.strftime('%Y-%m-%d %H:%M:%S',
                                                                   time.strptime(result['created_at'],
                                                                                 '%a %b %d %H:%M:%S +0000 %Y')),
                                     'date_fetched': datetime.datetime.now().strftime('%a %b %d %H:%M:%S +0000 %Y'),
                                     'has_media': has_media(result),
                                     'user': result['user'],
                                     'item_type': 'twitter_post'})


def get_media_url(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            if result['extended_entities']['media'][0]['type'] == 'video':
                return result['extended_entities']['media'][0]['video_info']['variants'][0]['url']
            else:
                return result['extended_entities']['media'][0]['media_url_https']
    if 'media' in result['entities']:
        return result['entities']['media'][0]['media_url']
    else:
        return None

def get_url_to_story(result):
    if 'urls' in result['entities']:
        if not result['entities']['urls']:
            return None
        else:
            return result['entities']['urls'][0]['url']
    else:
        return None

def get_media_https_url(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            if result['extended_entities']['media'][0]['type'] == 'video':
                return result['extended_entities']['media'][0]['video_info']['variants'][0]['url']
            else:
                return result['extended_entities']['media'][0]['media_url_https']
    elif 'media' in result['entities']:
        return result['entities']['media'][0]['media_url_https']
    else:
        return None

def get_media_type(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            return result['extended_entities']['media'][0]['type']
    elif 'media' in result['entities']:
        return result['entities']['media'][0]['type']
    else:
        return None

def get_media_sizes(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            return result['extended_entities']['media'][0]['sizes']
        else:
            return result['entities']['media'][0]['sizes']
    elif 'media' in result['entities']:
        return result['entities']['media'][0]['sizes']
    else:
        return None

def has_media(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            return True
        else:
            return False
    elif 'media' in result['entities']:
        return True
    else:
        return False
