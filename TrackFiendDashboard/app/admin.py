from django.contrib import admin
from .models import Artist, Account

class ArtistAdmin(admin.ModelAdmin):
    list_display = ('name', 'realname')
    search_fields = ('name', 'realname')

admin.site.register(Artist, ArtistAdmin)
admin.site.register(Account)
