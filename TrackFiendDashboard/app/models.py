from __future__ import unicode_literals

from django.db import models


class Artist(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    realname = models.CharField(max_length=255, blank=True, null=True)
    discogsid = models.CharField(max_length=255, blank=True, null=True)
    mbid = models.CharField(max_length=255, blank=True, null=True)
    cname = models.CharField(unique=True, max_length=255, blank=True, null=True)
    inserted_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    artist_image_url = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'artists'

class Account(models.Model):
    firstname = models.CharField(max_length=255, blank=True, null=True)
    lastname = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(unique=True, max_length=255)
    password_digest = models.CharField(max_length=255, blank=True, null=True)
    inserted_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    artists = models.ManyToManyField(Artist)

    def __unicode__(self):
        return self.firstname + self.lastname

    class Meta:
        db_table = 'accounts'

class SchemaMigrations(models.Model):
    version = models.BigIntegerField(primary_key=True)
    inserted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'schema_migrations'
