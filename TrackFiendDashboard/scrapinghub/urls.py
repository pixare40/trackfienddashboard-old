"""
Definition of urls for TrackFiendDashboard.
"""

from datetime import datetime
from django.conf.urls import url
import django.contrib.auth.views

import scrapinghub.views

# Uncomment the next lines to enable the admin:
from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', scrapinghub.views.scrapinghome, name='scraping_home'),
    url(r'^startscraper/(?P<scrapesite>\w+)/(?P<scrapecategory>\w+)$', scrapinghub.views.startscraper, name='start_scraper'),
    url(r'^twitter_hub', scrapinghub.views.twitter_dashboard, name="twitter_dashboard"),
    url(r'^fetchGoogleContent', scrapinghub.views.populate_artist_images, name="google_dash"),
    url(r'^fetchTwitterContent', scrapinghub.views.fetchTwitterContent, name="fetch_twitter_news"),
]

