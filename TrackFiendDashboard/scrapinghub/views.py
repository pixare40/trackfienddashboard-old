# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from .tasks import intiateHotNewHipHopArtistsCrawl
from .tasks import initiateFetchTwitterNews
from .tasks import populateArtistImages

def scrapinghome(request):
    return render(
        request,
        'admin/scraping/scraping_hub.html',)

def startscraper(request, scrapesite, scrapecategory):
    scrape_site()
    return render(
        request,
        'admin/scraping/scraping_initiated.html')

def scrape_site():
    intiateHotNewHipHopArtistsCrawl.delay()
    return True

def populate_artist_images(request):
    #initiate fetch artists images
    populateArtistImages.delay()
    return redirect("http://127.0.0.1:5555/dashboard")

def twitter_dashboard(request):
    return render(request, 'admin/socials/twitter/twitter_hub.html')

def fetchTwitterContent(request):
    #Initiate fetch news from twitter
    from .tasks import fetchTwitterNews
    fetchTwitterNews()
    initiateFetchTwitterNews.delay()
    return redirect("http://127.0.0.1:5555/dashboard")



