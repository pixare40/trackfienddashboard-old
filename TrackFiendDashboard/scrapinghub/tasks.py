from multiprocessing import Process
from celery import task
from app.models import Artist
from twython import Twython
import pymongo
import datetime
import json
import urllib

@task()
def intiateHotNewHipHopArtistsCrawl():
    p = Process(target=startCrawlingHotnewHiphop)
    p.start()
    p.join()

def startCrawlingHotnewHiphop():
    from artistbots.spiders import runner
    runner.run()

@task()
def runSentimentAnalysis():
    pass

@task()
def runArtistAnalysis():
    pass

@task
def initiateCollectBlogNews():
    p = Process(target=collectBlogNews)
    p.start()
    p.join()

def collectBlogNews():
    from newsbots.spiders import runner
    runner.run()
    return

def fetchTwitterNews():
    pass

def get_media_url(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            if result['extended_entities']['media'][0]['type'] == 'video':
                return result['extended_entities']['media'][0]['video_info']['variants'][0]['url']
            else:
                return result['extended_entities']['media'][0]['media_url_https']
    if 'media' in result['entities']:
        return result['entities']['media'][0]['media_url']
    else:
        return None

def get_url_to_story(result):
    if 'urls' in result['entities']:
        if not result['entities']['urls']:
            return None
        else:
            return result['entities']['urls'][0]['url']
    else:
        return None

def get_media_https_url(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            if result['extended_entities']['media'][0]['type'] == 'video':
                return result['extended_entities']['media'][0]['video_info']['variants'][0]['url']
            else:
                return result['extended_entities']['media'][0]['media_url_https']
    elif 'media' in result['entities']:
        return result['entities']['media'][0]['media_url_https']
    else:
        return None

def get_media_type(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            return result['extended_entities']['media'][0]['type']
    elif 'media' in result['entities']:
        return result['entities']['media'][0]['type']
    else:
        return None

def get_media_sizes(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            return result['extended_entities']['media'][0]['sizes']
        else:
            return result['entities']['media'][0]['sizes']
    elif 'media' in result['entities']:
        return result['entities']['media'][0]['sizes']
    else:
        return None

def has_media(result):
    if 'extended_entities' in result:
        if 'media' in result['extended_entities']:
            return True
        else:
            return False
    elif 'media' in result['entities']:
        return True
    else:
        return False

@task
def initiateFetchNewsStories():
    p = Process(target=startCrawlingNews)
    p.start()
    p.join()

@task
def initiateFetchTwitterNews():
    p = Process(target=fetchTwitterNews)
    p.start()
    p.join()

def startCrawlingNews():
    from newsbots.spiders import runner
    runner.run()

def fetchArtistImages():
    p = Process()
    p.start()
    p.join()

def populateArtistImages():
    all_artist = Artist.objects.all()
    API_KEY = 'AIzaSyAJGJ5Q2MIjJRwAaS3_BMX9bi5Eay-7IBw'
    for a in  all_artist:
        query = a.name.encode('utf8')
        service_url = 'https://kgsearch.googleapis.com/v1/entities:search'
        params = {
            'query': query,
            'limit': 10,
            'indent': True,
            'key': API_KEY,
        }
        url = service_url + '?' + urllib.urlencode(params)
        response = json.loads(urllib.urlopen(url).read())
        if 'itemListElement' in response and response['itemListElement']:
            if 'result' in response['itemListElement'][0]:
                if 'image' in response['itemListElement'][0]['result']:
                    fetched_artist_image_url = response['itemListElement'][0]['result']['image']['contentUrl']
                    Artist.objects.filter(name = a.name).update(artist_image_url=fetched_artist_image_url)
