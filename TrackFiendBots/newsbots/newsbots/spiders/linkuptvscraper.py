import sys
import scrapy
import datetime
from scrapy.selector import Selector
from newsbots.items import TrackFiendNewsItem
import unicodedata
import sys
from news_sources_value_object import NewsSourcesValueObject
from news_types_value_object import NewsTypesValueObject
from news_item_keys_value_object import NewsItemKeysValueObject

class LinkUpTVScraper(scrapy.Spider):
    name = "linkuptv"
    allowed_domains = ['thelinkup.com']
    base_url_to_follow = 'http://thelinkup.com/'
    start_urls = ['http://thelinkup.com/videos/news/']

    def parse(self, response):

        sel = Selector(response)

        news_items = sel.xpath('//div[@class="videodesccon"]')

        for item in news_items:
            news_item = TrackFiendNewsItem()
            url_to_article = item.xpath('string(.//a/@href)').extract_first().strip()

            news_item['url_to_story'] = url_to_article

            request = scrapy.Request(url=url_to_article, callback=self.parse_article)
            request.meta['item'] = news_item

            yield request

    def parse_article(self, response):

        news_item = response.meta['item']

        sel = Selector(response)
        news_item_title = sel.xpath('string(//div[@class="posttitletop"]/h1)').extract_first().strip()
        news_item_image = sel.xpath('string(//div[@class="fimgcon"]/img/@src)').extract_first().strip()
        story_paragraph_list = sel.xpath('//div[@class="maincontentcon"]/p/text()').extract()
        news_story = ' '.join(story_paragraph_list)
        current_datetime = datetime.datetime.now().strftime('%a %b %d %H:%M:%S +0000 %Y')

        tags_list = news_item_title
        tags = self.clean_tags(tags_list)

        news_item[NewsItemKeysValueObject.TITLE] = news_item_title
        news_item[NewsItemKeysValueObject.NEWS_STORY] = news_story
        news_item[NewsItemKeysValueObject.NEWS_ITEM_IMAGE] = news_item_image
        news_item[NewsItemKeysValueObject.DATE_CREATED] = current_datetime
        news_item[NewsItemKeysValueObject.DATE_FETCHED] = current_datetime
        news_item[NewsItemKeysValueObject.TAGS] = tags
        news_item[NewsItemKeysValueObject.ITEM_TYPE] = NewsTypesValueObject.ARTICLE
        news_item[NewsItemKeysValueObject.SOURCE] = NewsSourcesValueObject.LINKUPTV

        yield news_item

    def clean_tags(self, tag=""):
        tags_list = list()
        no_punctuation_tags = tag.split(' ')
        tbl = dict.fromkeys(i for i in xrange(sys.maxunicode)
                            if unicodedata.category(unichr(i)).startswith('P'))
        for index, dirty_tag in enumerate(no_punctuation_tags):
            clean_tag = dirty_tag.translate(tbl)
            first_tag = unicode.lower(unicode.strip(clean_tag))
            tags_list.append(first_tag)

            if index+1 < len(no_punctuation_tags):
                next_tag_to_merge = no_punctuation_tags[index+1]
                clean_next_tag = next_tag_to_merge.translate(tbl)
                second_tag = unicode.lower(unicode.strip(clean_next_tag))
                new_complete_tag = '%s %s' % (first_tag, second_tag)
                tags_list.append(new_complete_tag)

        return tags_list