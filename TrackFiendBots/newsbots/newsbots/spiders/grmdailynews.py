import sys
import scrapy
import codecs
import datetime
import re
import json
from newsbots.items import TrackFiendNewsItem
import unicodedata
import sys
from news_sources_value_object import NewsSourcesValueObject
from news_types_value_object import NewsTypesValueObject
from news_item_keys_value_object import NewsItemKeysValueObject

class GrmDailyNews(scrapy.Spider):
    name = "grmdailynews"
    allowed_domains = ['grmdaily.com']
    base_url_to_follow = 'http://grmdaily.com/umbraco/api/posts/resolve/'
    custom_settings = {"DEFAULT_REQUEST_HEADERS": {
      'Accept': 'application/json, text/plain, */*',
      'Accept-Language': 'en-US,en;q=0.8',
    }}
    start_urls = ['http://grmdaily.com/umbraco/api/posts?category=news&pageIndex=1&startIndex=0']

    def parse(self, response):

        news_stories = json.loads(response.body)

        for news_story in news_stories:
            news_item = TrackFiendNewsItem()
            relative_url_to_story = news_story['urlName']
            url_to_article = self.base_url_to_follow + relative_url_to_story

            news_item['url_to_story'] = url_to_article

            request = scrapy.Request(url=url_to_article, callback=self.parse_article)
            request.meta['item'] = news_item

            yield request

    def parse_article(self, response):
        news_item = response.meta['item']
        data = json.loads(response.body)

        news_item_title = data['content']['title']
        news_story = self.cleanhtml(data['content']['html'])
        current_datetime = datetime.datetime.now().strftime('%a %b %d %H:%M:%S +0000 %Y')
        news_item_image = data['content']['postImageUrl']

        tags_list = news_item_title
        tags = self.clean_tags(tags_list)

        news_item[NewsItemKeysValueObject.TITLE] = news_item_title
        news_item[NewsItemKeysValueObject.NEWS_STORY] = news_story
        news_item[NewsItemKeysValueObject.NEWS_ITEM_IMAGE] = news_item_image
        news_item[NewsItemKeysValueObject.DATE_CREATED] = current_datetime
        news_item[NewsItemKeysValueObject.DATE_FETCHED] = current_datetime
        news_item[NewsItemKeysValueObject.TAGS] = tags
        news_item[NewsItemKeysValueObject.ITEM_TYPE] = NewsTypesValueObject.ARTICLE
        news_item[NewsItemKeysValueObject.SOURCE] = NewsSourcesValueObject.GRM_DAILY

        yield news_item

    def clean_tags(self, tag=""):
        tags_list = list()
        no_punctuation_tags = tag.split(' ')
        tbl = dict.fromkeys(i for i in xrange(sys.maxunicode)
                            if unicodedata.category(unichr(i)).startswith('P'))
        for index, dirty_tag in enumerate(no_punctuation_tags):
            clean_tag = dirty_tag.translate(tbl)
            first_tag = unicode.lower(unicode.strip(clean_tag))
            tags_list.append(first_tag)

            if index+1 < len(no_punctuation_tags):
                next_tag_to_merge = no_punctuation_tags[index+1]
                clean_next_tag = next_tag_to_merge.translate(tbl)
                second_tag = unicode.lower(unicode.strip(clean_next_tag))
                new_complete_tag = '%s %s' % (first_tag, second_tag)
                tags_list.append(new_complete_tag)

        return tags_list

    def cleanhtml(self, raw_html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext