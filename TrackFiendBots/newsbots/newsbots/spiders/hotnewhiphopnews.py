# -*- coding: utf-8 -*-
import sys
import scrapy
import datetime
from scrapy.selector import Selector
from newsbots.items import TrackFiendNewsItem

class HotNewHipHopNewsSpider(scrapy.Spider):
    name = 'HotNewHipHopNews'
    allowed_domains = ['hotnewhiphop.com']
    base_url_to_start = 'http://www.hotnewhiphop.com'
    start_urls = ['http://www.hotnewhiphop.com/articles/news/']

    def parse(self, response):

        sel = Selector(response)
        news_items = sel.xpath('//li[@class="endlessScrollCommon-list-item rectangle-cover "]')

        for item in news_items:
            news_item = TrackFiendNewsItem()

            relative_url_to_story = item.xpath('string(.//a[@class="endlessScrollCommon-cover-anchor"]/@href)').extract_first()
            url_to_article = self.base_url_to_start + relative_url_to_story.strip();
            summary = item.xpath('string(.//div[@class="endlessScrollCommon-description hidden-md-down"])').extract_first().strip()
            
            news_item['summary'] = summary
            news_item['url_to_story'] = url_to_article

            request = scrapy.Request(url=url_to_article, callback= self.parse_article)
            request.meta['item'] = news_item

            yield request

    def parse_article(self, response):

        news_item = response.meta['item']
        sel = Selector(response)

        news_item_title = sel.xpath('string(//h1[@itemprop="headline"])').extract_first().strip()
        story_paragraph_list = sel.xpath('//section[@itemprop="articleBody"]/p/text()').extract()
        news_story = ' '.join(story_paragraph_list)
        current_datetime = datetime.datetime.now().strftime('%a %b %d %H:%M:%S +0000 %Y')
        news_item_image = sel.xpath('string(//img[@class="article-gallery-cover"]/@src)').extract_first()

        tags_list = sel.xpath('//span[@class="keywords"]/span/a/text()').extract()

        tags = map(self.clean_tags, tags_list)

        news_item['title'] = news_item_title
        news_item['news_story'] = news_story
        news_item['news_item_image'] = news_item_image.strip()
        news_item['date_created'] = current_datetime
        news_item['date_fetched'] = current_datetime
        news_item['tags'] = tags
        news_item['item_type'] = 'article'
        news_item['source'] = 'HNHH'

        yield news_item

    def clean_tags(self, tag=""):
        return unicode.lower(unicode.strip(tag))
