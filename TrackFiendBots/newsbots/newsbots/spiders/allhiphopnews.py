# -*- coding: utf-8 -*-

import sys
import scrapy
import codecs
import datetime
import lxml.html

from scrapy.selector import Selector
from newsbots.items import TrackFiendNewsItem

class AllNewHipHopNewsSpider(scrapy.Spider):
    name = "AllHipHopNews"
    allowed_domains = ["allhiphop.com"]
    start_urls = (
        'http://www.allhiphop.com/category/news/',
    )

    def parse(self, response):

        sel = Selector(response)
        articles = sel.xpath('//article[contains(@class, "post")]')

        for article in articles:
            news_item = TrackFiendNewsItem()

            url_to_article = article.xpath('string(.//a/@href)').extract_first()
            summary = article.xpath('string(.//section[contains(@class, "entry-content")]/p)').extract_first()
            
            news_item['summary'] = summary
            news_item['url_to_story'] = url_to_article

            request = scrapy.Request(url=url_to_article, callback= self.parse_article)
            request.meta['item'] = news_item

            yield request

    def parse_article(self, response):

        news_item = response.meta['item']

        sel = Selector(response)
        news_item_title = sel.xpath('string(//h1[@class="entry-title single-title"])').extract_first()
        
        news_story_paragraph_list = sel.xpath('//div[@itemprop="articleBody"]/p/text()').extract()
        news_story = ' '.join(news_story_paragraph_list)

        current_datetime = datetime.datetime.now().strftime('%a %b %d %H:%M:%S +0000 %Y')
        tags = sel.xpath('string(//meta[@itemprop="keywords"]/@content)').extract_first()
        fetched_tags_list = tags.split(',')

        news_item_image = sel.xpath('string(.//div[@class="article-img-holder"]/div/img/@src)').extract_first()

        tags_list = map(self.clean_tags, fetched_tags_list)

        news_item['title'] = news_item_title.strip()
        news_item['news_story'] = news_story.strip()
        news_item['news_item_image'] = news_item_image.strip()
        news_item['date_created'] = current_datetime
        news_item['date_fetched'] = current_datetime
        news_item['tags'] = tags_list
        news_item['item_type'] = 'article'
        news_item['source'] = 'AHH'

        yield news_item

    def clean_tags(self, tag=""):
        return unicode.lower(unicode.strip(tag))