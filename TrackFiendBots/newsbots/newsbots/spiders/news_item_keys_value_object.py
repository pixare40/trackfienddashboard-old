class NewsItemKeysValueObject(object):
    TITLE = "title"
    NEWS_STORY = "news_story"
    NEWS_ITEM_IMAGE = "news_item_image"
    DATE_CREATED = "date_created"
    DATE_FETCHED = "date_fetched"
    TAGS = "tags"
    ITEM_TYPE = "item_type"
    SOURCE = "source"