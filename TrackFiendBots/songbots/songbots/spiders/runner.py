from twisted.internet import reactor
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.log import configure_logging

def run():
    from hiphopdx import HipHopDXSpider
    from hotnewhiphopsongs import HotNewHipHopSongsSpider
    configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
    crawlerProcess = CrawlerProcess()
    crawlerProcess.crawl(HipHopDXSpider)
    crawlerProcess.crawl(HotNewHipHopSongsSpider)
    crawlerProcess.start()
    crawlerProcess.stop()

