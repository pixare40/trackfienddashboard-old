import scrapy
import lxml
from scrapy.selector import Selector
import datetime
from artistbots.items import ArtistItem

class MixtapeMadnessArtists(scrapy.Spider):
    name = "mixtapemadnessartists"
    allowed_domains = ["www.mixtapemadness.com"]
    base_url_to_start = "http://www.mixtapemadness.com"
    start_urls = ["http://www.mixtapemadness.com/artists"]

    def parse(self, response):
        sel = Selector(response)
        artists = sel.xpath('//a[@class="mixtape-thumb__title"]')

        for artist in artists:
            artist_item = dict()
            artist_name = artist.xpath('string(.//text())').extract_first().strip()

            artist_item["artist_name"] = artist_name
            artist_item["artist_cname"] = self.get_artist_cname(artist_name)

            yield ArtistItem(name=artist_name, cname=self.get_artist_cname(artist_name))

        next_page_url = sel.xpath('string(//a[@rel="next"]/@href)').extract_first()

        if next_page_url:
            next_page_request = scrapy.Request(url=next_page_url, callback=self.parse)

            yield next_page_request

    def get_artist_cname(self, artist_name=""):
        stripped_name = artist_name.strip()
        replace_spaces = stripped_name.replace(" ","_")

        return replace_spaces.lower()