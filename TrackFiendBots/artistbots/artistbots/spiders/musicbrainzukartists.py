import scrapy
import lxml
from scrapy.selector import Selector
import datetime

class MusicBrainzUkArtists(scrapy.Spider):
    name = "musicbrainzukartists"
    allowed_domains = ["musicbrainz.org"]
    base_url_to_start = "https://musicbrainz.org"
    start_urls = ["https://musicbrainz.org/area/8a754a16-0027-3a29-b6d7-2b40ea0481ed/artists"]

    def parse(self, response):

        sel = Selector(response)
        artists = sel.xpath("//tbody/tr")

        for artist in artists:
            artist_item = dict()
            artist_name = artist.xpath('string(.//td[1]/a/bdi)').extract_first()
            relative_url_to_artist = artist.xpath('string(.//td[1]/a/@href)').extract_first()
            url_to_artist = self.base_url_to_start + relative_url_to_artist

            artist_item['artist_name'] = artist_name
            artist_item['artist_cname'] = self.get_artist_cname(artist_name)

            yield artist_item

    def parse_artist_info(self, response):
        pass

    def get_artist_cname(self, artist_name=""):
        stripped_name = artist_name.strip()
        replace_spaces = stripped_name.replace(" ","_")

        return replace_spaces.lower()