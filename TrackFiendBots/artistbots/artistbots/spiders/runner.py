from twisted.internet import reactor
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from artistbots import settings
import sys
import os
import django

DJANGO_PROJECT_PATH = '../../../TrackFiendDashboard/TrackFiendDashboard'
DJANGO_SETTINGS_MODULE = 'TrackFiendDashboard.settings'

sys.path.insert(0, DJANGO_PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = DJANGO_SETTINGS_MODULE

django.setup()

def run():
    from hotnewhiphop import HotNewHipHopArtistsSpider
    from hiphopdx import HipHopDXSpider
    configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
    crawlerProcess = CrawlerProcess(get_project_settings())
    crawlerProcess.crawl(HotNewHipHopArtistsSpider)
    crawlerProcess.crawl(HipHopDXSpider)
    crawlerProcess.start()
    crawlerProcess.stop()
