# -*- coding: utf-8 -*-
import scrapy
import lxml
from scrapy.selector import Selector
import datetime
from artistbots.items import ArtistItem

class HipHopDXSpider(scrapy.Spider):
    name = 'hiphopdx'
    allowed_domains = ['hiphopdx.com']
    base_url_to_start = 'http://hiphopdx.com'
    start_urls = ['http://hiphopdx.com/singles']

    def parse(self, response):
        
        sel = Selector(response)
        artists = sel.xpath('//td[@class="artist"]')

        for artist in artists:
            artist_item = dict()
            artist_name = artist.xpath('string(.//text())').extract_first()
            cleaned_artist_name = self.get_cleaned_artist_name(artist_name).strip()

            artist_cname = self.get_artist_cname(artist_name=cleaned_artist_name)

            yield ArtistItem(name=cleaned_artist_name, cname=artist_cname)

        next_page = sel.xpath('string(//a[@class="next"]/@href)').extract_first()
        if next_page:
            next_page_url = self.base_url_to_start + next_page.strip()
            next_page_parse_request = scrapy.Request(url=next_page_url, callback=self.parse)

            yield next_page_parse_request

    def get_cleaned_artist_name(self, artist_name):
        splitters = ['f.',',','&']
        if any(splitter in artist_name for splitter in splitters):
            breaker = ""
            if splitters[0] in artist_name:
                breaker = splitters[0]
            elif splitters[1] in artist_name:
                breaker = splitters[1]
            elif splitters[2] in artist_name:
                breaker = splitters[2]

            return artist_name.split(breaker)[0]
        else:
            return artist_name

    def get_artist_cname(self, artist_name=""):
        stripped_name = artist_name.strip()
        replace_spaces = artist_name.replace(" ","_")
        return replace_spaces.lower()